# Changelog

* [Unreleased](#unreleased)
* [1.4.2](#1-4-2)
* [1.4.1](#1-4-1)


## Unreleased
### Added
### Deprecated
### Removed
### Fixed
### Security
### Contributors


## 1.4.2

### Fixed

* Subpixel antialiasing was not applied correctly on opaque
  backgrounds.


## 1.4.1

### Fixed

* Incorrect extension for man pages.
